.PHONY: build compile-dev-requirements install-dev-requirements check-type format lint sort-imports clean

build:
	python3 -m build

compile-dev-requirements:
	CUSTOM_COMPILE_COMMAND="make compile" pip-compile --generate-hashes --upgrade --strip-extras requirements-dev.in --output-file=requirements-dev.txt

install-dev-requirements:
	pip-sync requirements-dev.txt

check-type:
	mypy .

format:
	black .

lint:
	ruff check

sort-imports:
	ruff check --select I --fix

clean:
	rm -rf .mypy_cache .ruff_cache dist
