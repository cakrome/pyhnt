import argparse
import concurrent.futures
import json
import sys
import urllib.request

API_URL = "https://hacker-news.firebaseio.com/v0"


def get_story_list() -> list[int]:
    story_list_response = urllib.request.urlopen(url=f"{API_URL}/topstories.json")
    json_data_list: list[int] = json.loads(story_list_response.read())
    return json_data_list


def story_generator(story_id: int) -> str:
    story: str = ""
    story_response = urllib.request.urlopen(url=f"{API_URL}/item/{story_id}.json")
    json_data_item = json.loads(story_response.read())
    url: str = (
        f"Hacker News URL: https://news.ycombinator.com/item?id={str(json_data_item['id'])}"
    )
    if "url" in json_data_item:
        url_direct: str = f"Direct URL: {json_data_item['url']}"
    else:
        url_direct = "Direct URL: "
    if "descendants" in json_data_item:
        up: str = (
            f"{json_data_item['title']}\nScore: {str(json_data_item['score'])}   Comments: {str(json_data_item['descendants'])}   User: {json_data_item['by']}"
        )
    else:
        up = f"{json_data_item['title']}\nScore: {str(json_data_item['score'])}   Comments: 0   User: {json_data_item['by']}"

    story = f"{up}\n{url}\n{url_direct}\n"
    return story


def story_list_gen(num_of_stories: int) -> list[str]:
    story_id_list: list[int] = get_story_list()
    story_list: list[str] = []
    with concurrent.futures.ThreadPoolExecutor(max_workers=num_of_stories) as executor:
        future_to_story = {
            executor.submit(story_generator, index): index
            for index in story_id_list[0:num_of_stories]
        }
        for future in concurrent.futures.as_completed(future_to_story):
            ssst: int = future_to_story[future]
            try:
                data: str = future.result()
                story_list.append(data)
            except Exception as exc:
                story_list.append(f"{ssst!r} generated an exception: {exc}\n")
    return story_list


def args_handler():
    parser = argparse.ArgumentParser(
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "-N", "--num", type=int, default=20, help="Number of stories to fetch"
    )
    parser.add_argument(
        "-V", "--version", help="Display the version", action="store_true"
    )
    return parser.parse_args()


def main() -> None:
    args = args_handler()
    num_of_stories: int = args.num
    if num_of_stories <= 0:
        sys.exit("Number of stories to fetch cannot be less or equal to 0.")
    elif num_of_stories > 500:
        num_of_stories = 500
        print(f"Max is {num_of_stories} stories.\n")
    if args.version:
        print("pyhnt 0.1.1")
        sys.exit(0)
    sss_list: list[str] = story_list_gen(num_of_stories)
    # Remove the last new line from the last story
    sss_list[-1] = sss_list[-1].removesuffix("\n")
    for ind in range(len(sss_list)):
        print(f"{str(ind + 1)}. {sss_list[ind]}")


if __name__ == "__main__":
    main()
